# ROS2 Foxy ZED2 Yolo wrapper
This repo contains a ros2 wrapper for yolo. 
It was inspired by this [repo](https://github.com/ros2/openrobotics_darknet_ros).

The latter allows the publishing of vision_msgs/Detection2DArray, see [documentation](http://docs.ros.org/en/api/vision_msgs/html/msg/Detection2DArray.html) for more details. 

To be able to have images with the detected region,this [repo](https://github.com/ros2/detection_visualizer) was used. The published topic is said to be displayable in rviz2. **Note that it is not currently available.**

A launch file was created to launch both the detection and the detection visualizer. In the launch file, it is possible to remap topics to subscribe to the desired image topics. 
**Note that the type and [QoS setting](https://docs.ros.org/en/foxy/Concepts/About-Quality-of-Service-Settings.html) must be compatible for the publisher/subscriber to receive the data.** The ZED has set its durability to **VOLATILE** and reliability to **BEST_EFFORT**. Refer to documentation [table](https://docs.ros.org/en/foxy/Concepts/About-Quality-of-Service-Settings.html) to know what its compatible.

# Prerequisites:
#### [ZED Wrapper installation](https://github.com/stereolabs/zed-ros2-wrapper)
[ZED examples installation](https://github.com/stereolabs/zed-ros2-examples) (Optional but allows visualization with RVIZ2)
- ZED SDK
- CUDA
- [image_commons 3.0](https://github.com/ros-perception/image_common/releases/tag/3.0.0)
- diagnostic_updater : $ sudo apt-get install ros-foxy-diagnostic-updater
#### Darknet package 
[darknet vendor](https://github.com/ros2/darknet_vendor)


**ZED published topics :**

- rgb/image_rect_color:  (type sensor_msgs/msg/Image) 
- rgb/image_raw_color:  (type sensor_msgs/msg/Image) 
- right/image_rect_color:  (type sensor_msgs/msg/Image) 
- right/image_raw_color:  (type sensor_msgs/msg/Image) 
- left/image_rect_color:  (type sensor_msgs/msg/Image) 
- left/image_raw_color:  (type sensor_msgs/msg/Image) 

<br>see [documentation](https://www.stereolabs.com/docs/ros2/) for more details.

**Darknet visualizer published topics**

- ~/dbg_image (type sensor_msgs/msg/Image) 

### Pre-installation
Make sure all dependencies are installed. CUDA is expected to be located at /usr/local/cuda. If you have multiple CUDA version check with ZED Diagnostic which one is being used.
This repo was tested with 
- CUDA 11.0 
- ZED SDK 3.5
- Ubuntu 20.04 
- Ros2 Foxy

### Installation

`$ cd <colcon_workspace>/src # Access the source folder of your colcon workspace`
<br>`$ git clone git@gitlab.com:huynh-anh.le/ros2-foxy-zed2-yolo-wrapper.git`
<br>`$ cd <colcon_workspace> # Go back to root of your workspace`
<br>`$ colcon build --symlink-install --cmake-args=-DCMAKE_BUILD_TYPE=Release`
<br>`$ echo source $(pwd)/install/local_setup.bash >> ~/.bashrc # (optinal) add automatic sourcing to your bashrc`
<br>`$ source ~/.bashrc`

### Start the detection and detection vizualiser
To suscribe to the desired topic, change the launch file located in darknet_ros/launch/ named zed2_yolo_launch.py.
Replace the value of variable **image_sub** to the desired topic. By default it subscribes to **'/zed2/zed_node/rgb/image_rect_gray'**.
In one terminal, do the following commands
<br>`$ cd darknet_ros/config`
<br>`$ source install/setup.bash`
<br>`$ ros2 launch ../launch/zed2_yolo_launch.py`

##### In another terminal, start your ZED2 node.

`$ cd <colcon_workspace>/src `
<br>`$ cd zed-ros2-wrapper`
<br>`$ source install/setup.bash`
<br>`$ ros2 launch zed_wrapper zed2.launch.py`

##### If you wish to visualize with Rviz2. 

`$ cd <colcon_workspace>/src `
<br>`$ cd zed-ros2-wrapper`
<br>`$ source install/setup.bash`
<br>`$ ros2 launch zed_display_rviz2 display_zed2.launch.py`

#### Commands to debug/see what's happening.
These commands allows debugging of individual packages.

`$ros2 topic echo topicname # Allows to see the msg received `
<br>`$ ros2 topic list # outputs all the current topics `
<br>`$ ros2 topic info topicname --verbose #outputs all information about topic such as msg type and QoS settings. `
<br>`$ ros2 run detection_visualizer detection_visualizer # only runs the detections visualizer. This will show images with the bounding boxes`
<br>`$ ros2 run openrobotics_darknet_ros detector_node --ros-args -p "network.config:=./yolov3-tiny.cfg" -p "network.weights:=./yolov3-tiny.weights" -p "network.class_names:=./coco.names" -r /detector_node/images:=/zed2/zed_node/rgb/image_rect_gray # this command allows to run the detector alone and remap this subscribing topic to the zed stream`
FYI: Each command has to be done in a terminal. Dont forget to source yourt workspace. If you run the commands alone, you also need to launch the zed stream.
<br>` $ ros2 launch zed_wrapper zed2.launch.py`


