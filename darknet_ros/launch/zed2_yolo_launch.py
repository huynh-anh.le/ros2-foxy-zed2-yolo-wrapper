from pathlib import Path
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():

    image_sub='/zed2/zed_node/rgb/image_rect_gray'
    dir=Path().resolve().parent
    return LaunchDescription([
        Node(
            package='openrobotics_darknet_ros',
            executable='detector_node',
            parameters=[dir/f"config/detector_node_params.yaml"],
            remappings=[
	    ('/detector_node/images', image_sub),
	    ],
        ),
        Node(
            package='detection_visualizer',
            executable='detection_visualizer',
            remappings=[
	    ('/detection_visualizer/images', image_sub),
	    ],)
       
        ]
      )

